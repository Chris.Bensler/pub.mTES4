﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("mTES4 Manager")> 
<Assembly: AssemblyDescription("Multiple install management utility for Bethesda games that use the TES4 engine.")> 
<Assembly: AssemblyCompany("Creativeportal")> 
<Assembly: AssemblyProduct("mTES4 Manager")> 
<Assembly: AssemblyCopyright("Copyright © Creativeportal 2010")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("b6fc2b71-4c91-4ff4-a670-2453eaf8934f")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("0.6.1.93")> 
