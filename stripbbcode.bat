@echo off

REM : strip [i][/i]
replace.vbs %1 "\[i\]" ""
replace.vbs %1 "\[/i\]" ""

REM : strip [b][/b]
replace.vbs %1 "\[b\]" ""
replace.vbs %1 "\[/b\]" ""

REM : strip [size="*"][/size]
REM replace.vbs %1 "\[size=\x22[^\x22\[\]]*\x22\]" ""
replace.vbs %1 "\[size=[^\]]*\]" ""
replace.vbs %1 "\[/size\]" ""

REM : strip [color="*"][/color]
REM replace.vbs %1 "\[color=\x22[^\x22\[\]]*\x22\]" ""
replace.vbs %1 "\[color=[^\]]*\]" ""
replace.vbs %1 "\[/color\]" ""

REM : convert [url="$1"]$2[/url] to $2: $1
REM replace.vbs %1 "\[url=\x22([^\x22\[\]]*)\x22\](.*)\[/url\]" "$2: $1"
replace.vbs %1 "\[url=\x22?([^\x22\[\]]*)\x22?\](.*)\[/url\]" "$2: $1"

REM : delete [s]*[/s]
REM replace.vbs %1 "(^\s*)?\[s\].*\[/s\]((\s?$)|(\s?))" ""
replace.vbs %1 "\[s\]" ""
replace.vbs %1 "\[/s\]" ""
