﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class dlgDelete
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(dlgDelete))
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnDelete = New System.Windows.Forms.Button
        Me.cbGame = New System.Windows.Forms.CheckBox
        Me.cbProfile = New System.Windows.Forms.CheckBox
        Me.cbAppData = New System.Windows.Forms.CheckBox
        Me.cbTag = New System.Windows.Forms.CheckBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(267, 257)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnDelete.Location = New System.Drawing.Point(348, 257)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(75, 23)
        Me.btnDelete.TabIndex = 5
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'cbGame
        '
        Me.cbGame.AutoSize = True
        Me.cbGame.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbGame.Location = New System.Drawing.Point(110, 160)
        Me.cbGame.Name = "cbGame"
        Me.cbGame.Size = New System.Drawing.Size(157, 17)
        Me.cbGame.TabIndex = 1
        Me.cbGame.Text = "Delete the &Game folder"
        Me.cbGame.UseVisualStyleBackColor = True
        '
        'cbProfile
        '
        Me.cbProfile.AutoSize = True
        Me.cbProfile.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbProfile.Location = New System.Drawing.Point(110, 183)
        Me.cbProfile.Name = "cbProfile"
        Me.cbProfile.Size = New System.Drawing.Size(161, 17)
        Me.cbProfile.TabIndex = 2
        Me.cbProfile.Text = "Delete the &Profile folder"
        Me.cbProfile.UseVisualStyleBackColor = True
        '
        'cbAppData
        '
        Me.cbAppData.AutoSize = True
        Me.cbAppData.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAppData.Location = New System.Drawing.Point(109, 206)
        Me.cbAppData.Name = "cbAppData"
        Me.cbAppData.Size = New System.Drawing.Size(174, 17)
        Me.cbAppData.TabIndex = 3
        Me.cbAppData.Text = "Delete the &AppData folder"
        Me.cbAppData.UseVisualStyleBackColor = True
        '
        'cbTag
        '
        Me.cbTag.AutoSize = True
        Me.cbTag.Checked = True
        Me.cbTag.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbTag.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTag.Location = New System.Drawing.Point(110, 137)
        Me.cbTag.Name = "cbTag"
        Me.cbTag.Size = New System.Drawing.Size(132, 17)
        Me.cbTag.TabIndex = 0
        Me.cbTag.Text = "Delete the &Tag file"
        Me.cbTag.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(411, 109)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = resources.GetString("Label1.Text")
        '
        'dlgDelete
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(435, 292)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cbTag)
        Me.Controls.Add(Me.cbAppData)
        Me.Controls.Add(Me.cbProfile)
        Me.Controls.Add(Me.cbGame)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnCancel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "dlgDelete"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "mTES4: Delete Clone"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents cbGame As System.Windows.Forms.CheckBox
    Friend WithEvents cbProfile As System.Windows.Forms.CheckBox
    Friend WithEvents cbAppData As System.Windows.Forms.CheckBox
    Friend WithEvents cbTag As System.Windows.Forms.CheckBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip

End Class
