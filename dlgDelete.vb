﻿Imports System.Windows.Forms

Public Class dlgDelete
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub Checkbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbTag.CheckedChanged, cbGame.CheckedChanged, cbProfile.CheckedChanged, cbAppData.CheckedChanged
        Dim check As Boolean
        check = False
        check = check Or cbTag.Checked
        If cbGame.Checked Then
            check = True
            cbTag.Checked = True
        End If

        check = check Or cbProfile.Checked
        check = check Or cbAppData.Checked

        btnDelete.Enabled = check
    End Sub
End Class
