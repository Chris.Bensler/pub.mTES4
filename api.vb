﻿Imports System.IO

Module API
    Public Sub AddLaunchTool(ByRef ini As String(), ByVal game As String, ByVal title As String, ByVal path As String, Optional ByVal args As String = "", Optional ByVal icon As String = "")
        ini = ini_set(ini, game & " Tool", "sTitle", title, ini.Length)
        ini = ini_set(ini, game & " Tool", "sFilePath", path, ini.Length)
        If Not args.Equals("") Then
            ini = ini_set(ini, game & " Tool", "sArguments", args, ini.Length)
        End If
        If Not icon.Equals("") Then
            ini = ini_set(ini, game & " Tool", "sIconPath", icon, ini.Length)
        End If
    End Sub

    Public Sub Error_UniqueName(ByVal name As String, ByVal path As String)
        MsgBox("A folder named '" & name & "' already exists!" & vbCrLf _
                             & " in '" & path & "'" & vbCrLf _
                             & "Please choose a different name.")
    End Sub

    Public Sub PopulateClones(ByRef lb As ListBox, ByVal Installpath As String)
        Dim dirs, files, INI As String()
        Dim item, name As String
        Dim index As Integer
        Dim game As String

        index = lb.SelectedIndex
        item = lb.SelectedItem

        lb.Items.Clear()

        game = Main.cbGames.SelectedItem
        dirs = Directory.GetDirectories(Get_GamePath(""))
        For Each dpath As String In dirs
            files = Directory.GetFiles(dpath, "_mTES4 (*).ini")
            name = ""
            For Each fpath As String In files
                INI = ini_load(fpath)
                If game.Equals(ini_get(INI, "General", "sMTES4_Game", game), StringComparison.OrdinalIgnoreCase) Then
                    name = ini_get(INI, "General", "sMTES4_Clone", "")
                    If dpath.Equals(Get_GamePath(Installpath), StringComparison.OrdinalIgnoreCase) Then
                        lb.Items.Insert(0, name)
                    Else
                        lb.Items.Add(name)
                    End If
                    Exit For 'only recognize the first tag found
                End If
            Next
        Next
        If index >= 0 Then
            If lb.Items.Contains(item) Then lb.SelectedIndex = lb.Items.IndexOf(item)
        End If
        Main.NotifyIcon1.Text = Main.Text & " [" & Main.cbGames.SelectedItem & ":" & Main.lbClones.Items.Item(0) & "]"
    End Sub

    Public Function CloneIsUnique(ByVal name As String) As Boolean
        Dim dirinfo As DirectoryInfo
        Dim files As String()
        Dim game As String

        CloneIsUnique = False
        If Directory.Exists(Get_GamePath(name)) Then
            files = Directory.GetFiles(Get_GamePath(name), "_mTES4 (*).ini")
            If Not files.Length() = 0 Then
                MsgBox("'" & name & "' is already marked as a clone!")
                Exit Function
            End If
        End If

        ' check against all supported games for active clone names
        For i As Integer = 0 To Main.cbGames.Items.Count - 1
            game = Main.cbGames.Items.Item(i)
            dirinfo = My.Computer.FileSystem.GetDirectoryInfo(RegGet_InstallPath(game))
            ' if the games are installed in the same parent folder
            If dirinfo.Parent.FullName.Equals(Main.GamePath, StringComparison.OrdinalIgnoreCase) Then
                ' if the new clone name is the same as the active clone name for game[i]
                If name.Equals(Get_TrackActive(game), StringComparison.OrdinalIgnoreCase) Then
                    MsgBox("A clone named '" & name & "' already exits!")
                    Exit Function
                End If
            End If
        Next
        CloneIsUnique = True
    End Function

    Public Function ImportClone() As Boolean
        Dim dirinfo As DirectoryInfo
        Dim result As MsgBoxResult
        Dim src As String

        ImportClone = False

        Main.FolderBrowserDialog1.SelectedPath = Get_GamePath(Main.InstallPath)
        Main.FolderBrowserDialog1.ShowNewFolderButton = False
        Main.FolderBrowserDialog1.Description = "Select the folder you want to import as a new clone."
        Main.DialogResult = Main.FolderBrowserDialog1.ShowDialog()
        If Main.DialogResult = Windows.Forms.DialogResult.Cancel Then Exit Function

        src = Main.FolderBrowserDialog1.SelectedPath
        dirinfo = My.Computer.FileSystem.GetDirectoryInfo(src)

        dlgClone.CloneSrc = ""
        dlgClone.CloneDst = InputBox("Enter a unique name for the new clone." & vbCrLf _
                                   & "Cancelling will use the folder name of the imported clone." _
                                   , "mTES4: Importing Clone from '" & src & "'")
        If dlgClone.CloneDst.Length() = 0 Then ' cancelled
            dlgClone.CloneDst = dirinfo.Name
        End If

        If Not CloneIsUnique(dlgClone.CloneDst) Then Exit Function

        result = MsgBox("Import '" & dlgClone.CloneDst & "' now?", MsgBoxStyle.OkCancel)
        If Not result = MsgBoxResult.Ok Then Exit Function ' cancelled

        ' Is a Dummy Clone required?
        If Not dirinfo.Parent.FullName.Equals(Get_GamePath(""), StringComparison.OrdinalIgnoreCase) Then
            Directory.CreateDirectory(Get_GamePath(dlgClone.CloneDst))
        End If

        ImportClone = (dlgClone.ShowDialog() = Windows.Forms.DialogResult.OK)
    End Function

    Public Function CopyClone() As Boolean
        Dim result As MsgBoxResult

        CopyClone = False
        If Main.lbClones.SelectedIndex = -1 Then Exit Function

        dlgClone.CloneSrc = Main.lbClones.SelectedItem
        result = MsgBox("You are about to create a new clone from '" & dlgClone.CloneSrc & "'." & vbCrLf _
             & "Continue?" _
              , MsgBoxStyle.OkCancel)
        If Not result = MsgBoxResult.Ok Then Exit Function ' cancelled

        dlgClone.CloneDst = InputBox("Enter a unique name for the new clone.", "mTES4: Creating Clone from '" & dlgClone.CloneSrc & "'")
        If dlgClone.CloneDst.Length() = 0 Then Exit Function ' cancelled

        If Not CloneIsUnique(dlgClone.CloneDst) Then Exit Function

        CopyClone = (dlgClone.ShowDialog() = Windows.Forms.DialogResult.OK)
    End Function

    Public Function SwitchClone(ByVal dst As String) As Boolean
        Dim GameSrc, GameDst As String
        Dim ProfileSrc, ProfileDst As String
        Dim AppDataSrc, AppDataDst As String
        Dim state As Integer
        Dim undo As Boolean

        SwitchClone = False

        GameSrc = Get_GamePath(Main.InstallPath)
        GameDst = Get_GamePath(dst)

        ProfileSrc = Get_ProfilePath(Main.cbGames.SelectedItem)
        ProfileDst = Get_ProfileClonePath(dst)

        AppDataSrc = Get_AppDataPath(Main.cbGames.SelectedItem)
        AppDataDst = Get_AppDataClonePath(dst)

        state = 0
        undo = False
        While state < 6
            state += 1
            Try
                Select Case state
                    Case 1
                        Directory.Move(GameSrc, Get_GamePath(Main.TrackActive))
                    Case 2
                        Directory.Move(GameDst, GameSrc)
                    Case 3
                        Directory.Move(ProfileSrc, Get_ProfileClonePath(Main.TrackActive))
                    Case 4
                        Directory.Move(ProfileDst, ProfileSrc)
                    Case 5
                        Directory.Move(AppDataSrc, Get_AppDataClonePath(Main.TrackActive))
                    Case 6
                        Directory.Move(AppDataDst, AppDataSrc)
                End Select
            Catch ex As Exception
                MsgBox("Unable to switch to '" & dst & "'!" & vbCrLf _
                      & "Make sure there are no relevant applications, files or folders open." & vbCrLf _
                      & "Some common causes of this are:" & vbCrLf _
                      & "1. If the game is still running." & vbCrLf _
                      & "2. If any tools are open, such as OBMM or Wrye Bash." & vbCrLf _
                      & "3. Any of the clone folders are open in Windows Explorer." & vbCrLf _
                      & "4. A file is open for editing." & vbCrLf _
                      & "5. A folder is set to the current directory in cmd.exe.")
                undo = True
                Exit While
            End Try
        End While
        If Not undo Then
            Main.TrackActive = Set_TrackActive(Main.cbGames.SelectedItem, dst)
            SwitchClone = True
            Exit Function
        End If

        While state
            Try
                Select Case state
                    Case 1
                        Directory.Move(Get_GamePath(Main.TrackActive), GameSrc)
                    Case 2
                        Directory.Move(GameSrc, GameDst)
                    Case 3
                        Directory.Move(Get_ProfileClonePath(Main.TrackActive), ProfileSrc)
                    Case 4
                        Directory.Move(ProfileSrc, ProfileDst)
                    Case 5
                        Directory.Move(Get_AppDataClonePath(Main.TrackActive), AppDataSrc)
                    Case 6
                        Directory.Move(AppDataSrc, AppDataDst)
                End Select
            Catch ex As Exception
            End Try
            state -= 1
        End While
    End Function

    Public Sub RenameTag(ByVal folder As String, ByVal name As String)
        Dim files, ini As String()

        ' delete old tags
        files = Directory.GetFiles(Get_GamePath(folder), "_mTES4 (*).ini")
        For Each src As String In files
            File.Delete(src)
        Next

        ini = ini_load(Get_GamePath(folder & "\_mTES4 (" & name & ").ini"))
        ini = ini_set(ini, "General", "sMTES4_Game", Main.cbGames.SelectedItem)
        ini = ini_set(ini, "General", "sMTES4_Clone", name)
        ini_save(Get_GamePath(folder & "\_mTES4 (" & name & ").ini"), ini)
    End Sub

    Public Function RenameClone() As Boolean
        Dim oldName, newName As String

        RenameClone = False
        If Main.lbClones.SelectedIndex = -1 Then Exit Function

        Do
            oldName = Main.lbClones.SelectedItem
            newName = InputBox("Renaming '" & oldName & "' to", "Renaming '" & oldName & "'", oldName)
            If newName.Equals("") Then Exit Function
            If newName.Equals(oldName, StringComparison.OrdinalIgnoreCase) Then Exit Function

            If Not CloneIsUnique(newName) Then
                Error_UniqueName(newName, Get_GamePath(""))
                Continue Do
            End If

            ' if the selected clone is not active
            If Not oldName.Equals(Main.TrackActive, StringComparison.OrdinalIgnoreCase) Then
                If Directory.Exists(Get_ProfileClonePath(newName)) Then
                    Error_UniqueName(newName, Get_ProfileClonePath(""))
                    Continue Do
                ElseIf Directory.Exists(Get_AppDataClonePath(newName)) Then
                    Error_UniqueName(newName, Get_AppDataClonePath(""))
                    Continue Do
                End If
            End If

            Exit Do
        Loop

        ' update TrackActive if this is the active clone
        If oldName.Equals(Main.TrackActive, StringComparison.OrdinalIgnoreCase) Then
            RenameTag(Main.InstallPath, newName) ' rename tag
            Main.TrackActive = Set_TrackActive(Main.cbGames.SelectedItem, newName)
        Else ' the selected clone is not active
            Directory.Move(Get_GamePath(oldName), Get_GamePath(newName)) ' rename game folder
            Directory.Move(Get_ProfileClonePath(oldName), Get_ProfileClonePath(newName)) ' rename profile folder
            Directory.Move(Get_AppDataClonePath(oldName), Get_AppDataClonePath(newName)) ' rename appdata folder
            RenameTag(newName, newName) ' rename tag
        End If
        RenameClone = True
    End Function

    Public Function DeleteClone() As Boolean
        Dim name, msg As String

        DeleteClone = False
        If Main.lbClones.SelectedIndex = -1 Then Exit Function

        name = Main.lbClones.SelectedItem

        dlgDelete.Text = "mTES4: Deleting '" & name & "'"

        dlgDelete.cbTag.Checked = True
        dlgDelete.cbGame.Checked = False
        dlgDelete.cbProfile.Checked = False
        dlgDelete.cbAppData.Checked = False

        dlgDelete.ToolTip1.SetToolTip(dlgDelete.cbTag, _
                "path to delete:" & vbCrLf _
              & "  '" & Get_GamePath(name & "\_mTES4 (" & name & ").ini") & "'" & vbCrLf _
              & vbCrLf _
              & "description:" & vbCrLf _
              & "  The mTES4 tag file keeps track of the name and other specific settings for each clone." & vbCrLf _
              & "  Removing the mTES4 tag file will prevent mTES4 from detecting this clone." & vbCrLf _
              & "  If you wanted to use this clone again afterwards you would have to re-import it.")
        dlgDelete.ToolTip1.SetToolTip(dlgDelete.cbGame, _
                "path to delete:" & vbCrLf _
              & "  '" & Get_GamePath(name) & "'" & vbCrLf _
              & vbCrLf _
              & "description:" & vbCrLf _
              & "  The Game folder contains your actual game." & vbCrLf _
              & "  If you delete this, you will be unable to use this clone again!")
        dlgDelete.ToolTip1.SetToolTip(dlgDelete.cbProfile, _
                "path to delete:" & vbCrLf _
              & "  '" & Get_ProfileClonePath(name) & "'" & vbCrLf _
              & vbCrLf _
              & "description:" & vbCrLf _
              & "  The Profile folder contains all the savegames, oblivion.ini, certain bash settings files and various other settings." & vbCrLf _
              & "  If you delete the profile folder, you will lose all the savegame files and game settings for this clone!" & vbCrLf _
              & "  It is recommended that you do not delete the Profile folder unless you are sure there is nothing you wish to keep.")
        dlgDelete.ToolTip1.SetToolTip(dlgDelete.cbAppData, _
                "path to delete:" & vbCrLf _
              & "  '" & Get_AppDataClonePath(name) & "'" & vbCrLf _
              & vbCrLf _
              & "description:" & vbCrLf _
              & "  The AppData folder contains the list of active plugins for this clone." & vbCrLf _
              & "  It may also contain various settings files for 3rd party tools such as TES4Edit and TES4Gecko." & vbCrLf _
              & "  It is suggested that you keep this folder if you want to be able to rebuid this clone.")

        Main.DialogResult = dlgDelete.ShowDialog()
        If Main.DialogResult = DialogResult.Cancel Then Exit Function

        msg = "You are about to delete '" & name & "'." & vbCrLf _
            & "The following files and folders will be permanently deleted!" & vbCrLf _
            & vbCrLf
        If dlgDelete.cbGame.Checked Then
            msg &= """" & Get_GamePath(name) & """" & vbCrLf
        ElseIf dlgDelete.cbTag.Checked Then
            msg &= """" & Get_GamePath(name & "\_mTES4 (" & name & ").ini") & """" & vbCrLf
        End If
        If dlgDelete.cbProfile.Checked Then
            msg &= """" & Get_ProfileClonePath(name) & """" & vbCrLf
        End If
        If dlgDelete.cbAppData.Checked Then
            msg &= """" & Get_AppDataClonePath(name) & """" & vbCrLf
        End If
        msg &= vbCrLf _
            & vbCrLf _
            & "Are you sure you want to continue?"
        If MsgBox(msg, MsgBoxStyle.OkCancel, "Deleting '" & name & "'") = MsgBoxResult.Cancel Then Exit Function

        Try
            If dlgDelete.cbGame.Checked Then
                Directory.Move(Get_GamePath(name), Get_GamePath("_DELETING" & name))
            ElseIf dlgDelete.cbTag.Checked Then
                File.Move(Get_GamePath(name & "\_mTES4 (" & name & ").ini"), Get_GamePath(name & "\_DELETING _mTES4 (" & name & ").ini"))
            End If
            If dlgDelete.cbProfile.Checked Then
                Directory.Move(Get_ProfileClonePath(name), Get_ProfileClonePath("_DELETING" & name))
            End If
            If dlgDelete.cbAppData.Checked Then
                Directory.Move(Get_AppDataClonePath(name), Get_AppDataClonePath("_DELETING" & name))
            End If
        Catch ex As Exception
            If dlgDelete.cbAppData.Checked Then
                Directory.Move(Get_AppDataClonePath("_DELETING" & name), Get_AppDataClonePath(name))
            End If
            If dlgDelete.cbProfile.Checked Then
                Directory.Move(Get_ProfileClonePath("_DELETING" & name), Get_ProfileClonePath(name))
            End If
            If dlgDelete.cbGame.Checked Then
                Directory.Move(Get_GamePath("_DELETING" & name), Get_GamePath(name))
            ElseIf dlgDelete.cbTag.Checked Then
                File.Move(Get_GamePath(name & "\_DELETING _mTES4 (" & name & ").ini"), Get_GamePath(name & "\_mTES4 (" & name & ").ini"))
            End If
            Exit Function
        End Try

        If dlgDelete.cbGame.Checked Then
            Directory.Delete(Get_GamePath("_DELETING" & name), True)
        ElseIf dlgDelete.cbTag.Checked Then
            File.Delete(Get_GamePath(name & "\_DELETING _mTES4 (" & name & ").ini"))
        End If
        If dlgDelete.cbProfile.Checked Then
            Directory.Delete(Get_ProfileClonePath("_DELETING" & name), True)
        End If
        If dlgDelete.cbAppData.Checked Then
            Directory.Delete(Get_AppDataClonePath("_DELETING" & name), True)
        End If

        DeleteClone = True
    End Function
End Module
