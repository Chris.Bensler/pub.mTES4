﻿Imports System.Windows.Forms
Imports System.IO

Public Class dlgAutoLaunch
    Private startinfo As ProcessStartInfo

    Private Sub dlgAutoLaunch_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dirinfo As DirectoryInfo
        Dim ini As String()
        ini = ini_load(Get_GamePath(Main.InstallPath) & "\_mTES4 (" & Main.TrackActive & ").ini")
        startinfo = New ProcessStartInfo()
        startinfo.FileName = ini_get(ini, "AutoLaunch", "sFilePath", "")
        startinfo.FileName = IO.Path.Combine(Get_GamePath(Main.InstallPath), startinfo.FileName)
        startinfo.Arguments = ini_get(ini, "AutoLaunch", "sArguments", "")
        dirinfo = My.Computer.FileSystem.GetDirectoryInfo(startinfo.FileName)
        startinfo.WorkingDirectory = dirinfo.Parent.FullName
        If String.Equals("True", ini_get(ini, "AutoLaunch", "bLaunchAlways", "False")) Then Launch()
    End Sub

    Private Sub btnLaunch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLaunch.Click
        Launch()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnTools_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTools.Click
        dlgLaunch.ShowDialog()
        Me.Close()
    End Sub

    Private Sub Launch()
        Main.WindowState = FormWindowState.Minimized
        Process.Start(startinfo)
        Me.Close()
    End Sub
End Class
