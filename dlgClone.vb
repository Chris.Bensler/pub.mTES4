﻿Imports System.Windows.Forms
Imports System.IO

Public Class dlgClone
    Public CloneSrc As String
    Public CloneDst As String

    Private GameSrc, GameDst As String
    Private ProfileSrc, ProfileDst As String
    Private AppDataSrc, AppDataDst As String

    Dim useProfileDst, useAppDataDst As Boolean

    Private done As Boolean

    Private Sub dlgCMconfirm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        done = False

        GameSrc = Get_GamePath(CloneSrc)
        GameDst = Get_GamePath(CloneDst)

        ProfileSrc = Get_ProfilePath(CloneSrc)
        ProfileDst = Get_ProfileClonePath(CloneDst)

        AppDataSrc = Get_AppDataPath(CloneSrc)
        AppDataDst = Get_AppDataClonePath(CloneDst)

        If CloneSrc.Equals("") Then
            ProfileSrc = Get_ProfilePath(Main.cbGames.SelectedItem)
            AppDataSrc = Get_AppDataPath(Main.cbGames.SelectedItem)
            If Not Test_Clone() Then Exit Sub
            Clone()
            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
            Exit Sub
        ElseIf CloneSrc.Equals(Main.TrackActive, StringComparison.OrdinalIgnoreCase) Then
            GameSrc = Get_GamePath(Main.InstallPath)
            ProfileSrc = Get_ProfilePath(Main.cbGames.SelectedItem)
            AppDataSrc = Get_AppDataPath(Main.cbGames.SelectedItem)
        End If

        lblSrcPath.Text = GameSrc
        lblDstCopy.Text = GameDst
        lblTip.Text = "Click Next to begin."
        ButtonNext.Text = "&Next"
        ButtonBack.Text = "&Back"
    End Sub

    Private Sub ButtonNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonNext.Click
        If Not done Then
            Me.DialogResult = Windows.Forms.DialogResult.None
            If Not Test_Clone() Then Exit Sub
            Clone()
        Else
            Me.Close()
        End If
    End Sub

    Private Sub ButtonBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBack.Click
        If done Then Undo_Clone()
        Me.Close()
    End Sub

    Private Sub Error_Clone(ByVal msg As String, ByVal tip As String)
        MsgBox(msg, MsgBoxStyle.Critical, "Unable to create clone!")
        If Not tip.Equals(String.Empty) Then tip &= vbCrLf
        lblTip.Text = "Unable to create clone!" & vbCrLf _
                    & tip _
                    & vbCrLf _
                    & vbCrLf _
                    & "Click Next to retry."
    End Sub

    Private Function Test_SrcDir(ByVal src As String, ByVal name As String) As Boolean
        Test_SrcDir = True
        If Not Directory.Exists(src) Then
            Error_Clone("""" & src & """ does not exist!", "Source " & name & " folder does not exist.")
            Test_SrcDir = False
        End If
    End Function

    Private Function Test_DstDir(ByVal dst As String, ByVal name As String, Optional ByVal abort As Boolean = True) As Boolean
        Dim msg, title As String
        Test_DstDir = True
        If Directory.Exists(dst) Then
            msg = """" & dst & """ already exists!"
            title = "Destination " & name & " folder already exists."
            If Not abort Then
                Me.DialogResult = MsgBox(msg & vbCrLf & "Do you want to use the existing folder?", MsgBoxStyle.YesNo, title)
                Test_DstDir = (Me.DialogResult = Windows.Forms.DialogResult.Yes)
                Me.DialogResult = Windows.Forms.DialogResult.None
                If Test_DstDir Then
                    If dst.Equals(ProfileDst) Then
                        useProfileDst = True
                    ElseIf dst.Equals(AppDataDst) Then
                        useAppDataDst = True
                    End If
                End If
            End If
            If Test_DstDir = False Then Error_Clone(msg, title)
        End If
    End Function

    Private Function CopyDir(ByVal src As String, ByVal dst As String) As Boolean
        CopyDir = True
        Try
            My.Computer.FileSystem.CopyDirectory(src, dst, FileIO.UIOption.AllDialogs)
        Catch ex As Exception
            lblTip.Text = "Clone Operation Cancelled." & vbCrLf _
                        & vbCrLf _
                        & vbCrLf _
                        & "Click Next to retry."
            CopyDir = False
        End Try
    End Function

    Private Function Test_Clone() As Boolean
        ' test game folder
        ' test profile folder
        ' test appdata folder
        Test_Clone = True
        useProfileDst = False
        useAppDataDst = False
        If CloneSrc.Equals("") Then
            If Not Test_SrcDir(GameDst, "game") Then
                Test_Clone = False
            End If
        Else
            If Not Test_SrcDir(GameSrc, "game") Then
                Test_Clone = False
            ElseIf Not Test_DstDir(GameDst, "game") Then
                Test_Clone = False
            End If
        End If
        If Test_Clone = False Then Exit Function

        If Not Test_DstDir(ProfileDst, "profile", False) Then
            Test_Clone = False
        ElseIf Not Test_DstDir(AppDataDst, "application data", False) Then
            Test_Clone = False
        End If
    End Function

    Private Function CloneFolder(ByVal src As String, ByVal dst As String, ByVal name As String, ByVal msg As String)
        If Directory.Exists(src) _
        And MsgBox("Do you want to copy the " & name & " folder from " & src & "?" & vbCrLf _
                 & msg _
                 , MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            If Not CopyDir(src, dst) Then ' Copy folder
                Return False
            End If
        Else ' Create new folder
            Directory.CreateDirectory(dst)
        End If
        Return True
    End Function

    Private Sub Clone()
        Dim ini As String()
        Dim dlgFolder As FolderBrowserDialog
        Dim game As String

        lblTip.Text = "Cloning in Progress"
        ' copy game folder
        If Not CloneSrc.Equals("") And Not CopyDir(GameSrc, GameDst) Then
            Undo_Clone()
            Exit Sub
        End If

        ' copy profile folder
        If Not useProfileDst Then
            If Not CloneFolder(ProfileSrc, ProfileDst _
                    , "game profile" _
                    , "The game profile folder contains various game and tool settings as well as all the savegame files.") Then
                Undo_Clone()
                Exit Sub
            End If
        End If

        ' copy appdata folder
        If Not useAppDataDst Then
            If Not CloneFolder(AppDataSrc, AppDataDst _
                    , "appdata" _
                    , "The appdata folder contains various game and tool settings.") Then
                Undo_Clone()
                Exit Sub
            End If
        End If

        RenameTag(GameDst, CloneDst)

        ' bash
        game = Main.cbGames.SelectedItem
        If Directory.Exists(Get_GamePath(CloneDst & "\mopy")) Then ' bash found
            Do
                If MsgBox("Bash detected." & vbCrLf _
                        & "Do you want to configure a private BAIN Mods folder for this clone?" & vbCrLf _
                        & "If you don't create a private mod folder, the path that is already set in bash.ini will be used." _
                        , MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                    If File.Exists(Get_GamePath(CloneDst & "\mopy\bash.ini")) Then
                        ini = ini_load(Get_GamePath(CloneDst & "\mopy\bash.ini"))
                    Else
                        ini = ini_load(Get_GamePath(CloneDst & "\mopy\bash_default.ini"))
                    End If
                    dlgFolder = New FolderBrowserDialog()
                    dlgFolder.SelectedPath = Get_GamePath("")
                    dlgFolder.ShowNewFolderButton = True
                    Me.DialogResult = dlgFolder.ShowDialog()
                    If Me.DialogResult = Windows.Forms.DialogResult.Cancel Then Continue Do

                    ini = ini_set(ini, "General", "s" & game & "Mods", dlgFolder.SelectedPath)
                    ini_save(Get_GamePath(CloneDst & "\mopy\bash.ini"), ini)
                End If
                Exit Do
            Loop
        End If
        Me.DialogResult = Windows.Forms.DialogResult.None

        done = True
        ButtonNext.Text = "&Finish"
        ButtonBack.Text = "&Cancel"
        lblTip.Text = "Clone Operation Completed." & vbCrLf _
                    & vbCrLf _
                    & vbCrLf _
                    & "Clicking Cancel will undo all clone operations."
    End Sub

    Private Sub Undo_Clone()
        Try
            If Not CloneSrc.Equals("") Then Directory.Delete(GameDst, True)
            If Not useProfileDst Then Directory.Delete(ProfileDst, True)
            If Not useAppDataDst Then Directory.Delete(AppDataDst, True)
        Catch ex As Exception
        End Try
        done = False
    End Sub
End Class
