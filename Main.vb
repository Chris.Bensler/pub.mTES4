﻿Imports System.IO

Public Class Main
    Public GamePath, InstallPath, TrackActive, mTES4_Game As String
    Private TrayMenu As ContextMenu

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim game, gamepath As String
        Dim glist As New ArrayList
        Dim detected As Boolean
        Dim ini As String()
        Dim offset As Integer

        Me.Text = "mTES4 " & My.Settings.VERSION

        ' prepare tray menus
        TrayMenu = New ContextMenu()
        TrayMenu.MenuItems.Add("&Games", New MenuItem() {})
        TrayMenu.MenuItems.Add("&Clones", New MenuItem() {})
        TrayMenu.MenuItems.Add("-")
        TrayMenu.MenuItems.Add("&Open", AddressOf TrayMenu_Click)
        TrayMenu.MenuItems.Add("E&xit", AddressOf TrayMenu_Click)

        TrayMenu.MenuItems.Item(0).MenuItems.Add(" ")
        AddHandler TrayMenu.MenuItems.Item(0).Popup, AddressOf TrayMenuGames_Popup
        TrayMenu.MenuItems.Item(1).MenuItems.Add(" ")
        AddHandler TrayMenu.MenuItems.Item(1).Popup, AddressOf TrayMenuClones_Popup
        TrayMenu.MenuItems.Item(2).Enabled = False

        NotifyIcon1.ContextMenu = TrayMenu

        Console.WriteLine("mTES4 Initializing")
        Console.WriteLine(System.Environment.OSVersion.VersionString & IIf(IsWow64(), " x64", " x32"))
        Console.WriteLine()
        Console.WriteLine("Detecting Supported Games")

        'Detect Supported Games
        detected = False
        For i As Integer = cbGames.Items.Count - 1 To 0 Step -1
            game = cbGames.Items.Item(i)
            Console.Write("Checking for " & game & " ... ")
            gamepath = RegGet_InstallPath(game)
            If gamepath Is Nothing Then gamepath = ""
            If gamepath.Equals("") Then
                Console.WriteLine("Not Installed")
                cbGames.Items.Remove(game)
            ElseIf Not Directory.Exists(gamepath) Then
                Console.WriteLine("Missing!")
                Console.WriteLine("gamepath = """ & gamepath & """")
                MsgBox(game & " appears to be installed, but the game folder is missing." & vbCrLf _
                     & "If you have renamed your " & game & " folder, you must rename it back to the name it was installed as.")
                cbGames.Items.Remove(game)
            Else
                detected = True
                Console.WriteLine("Detected")
                Console.WriteLine("gamepath = """ & gamepath & """")
                ' create mTES4 folders if they don't exist
                Directory.CreateDirectory(Get_ProfilePath(game & "-mTES4"))
                Directory.CreateDirectory(Get_AppDataPath(game & "-mTES4"))
                'End If
            End If
        Next
        If Not detected Then
            Console.WriteLine("No Supported Games Detected!")
            Console.WriteLine("mTES4 path = """ & Get_GamePath("") & """")
            MsgBox("You must install Oblivion, Fallout3 or Fallout:New Vegas before using mTES4.", MsgBoxStyle.Critical, "No Game Installed!")
            Me.Close()
            Exit Sub
        End If

        Console.WriteLine()
        Console.WriteLine("Initialized")

        If Not File.Exists(Get_AppPath("mTES4.ini")) Then ' generate a new ini
            ini = ini_load(Get_AppPath("mTES4.ini"))

            ini = ini_set(ini, "General", "sMTES4_TrackGame", "")
            ini = ini_set(ini, "Oblivion", "sMTES4_TrackActive", "")
            ini = ini_set(ini, "Fallout3", "sMTES4_TrackActive", "")
            ini = ini_set(ini, "FalloutNV", "sMTES4_TrackActive", "")

            ' Oblivion Tools
            AddLaunchTool(ini, "Oblivion", "Wrye Bash", "Mopy\Wrye Bash Launcher.pyw")
            AddLaunchTool(ini, "Oblivion", "OBMM", "OblivionModManager.exe")
            AddLaunchTool(ini, "Oblivion", "OblivionLauncher", "OblivionLauncher.exe")
            AddLaunchTool(ini, "Oblivion", "Oblivion", "Oblivion.exe")
            AddLaunchTool(ini, "Oblivion", "TES4CS", "TESConstructionSet.exe")
            AddLaunchTool(ini, "Oblivion", "TES4Edit", "TES4Edit.exe")

            ' Fallout3 Tools
            AddLaunchTool(ini, "Fallout3", "Wrye Flash", "Mopy\Wrye Bash Launcher.pyw")
            AddLaunchTool(ini, "Fallout3", "FOMM", "fomm\fomm.exe")
            AddLaunchTool(ini, "Fallout3", "Fallout3", "Fallout3.exe")
            AddLaunchTool(ini, "Fallout3", "Fallout3 Launcher", "Fallout3Launcher.exe")
            AddLaunchTool(ini, "Fallout3", "GECK", "Geck.exe")
            AddLaunchTool(ini, "Fallout3", "FO3Edit", "FO3Edit.exe")
            AddLaunchTool(ini, "Fallout3", "FO3Plugin", "FO3Plugin.jar", "-Xmx1024m", "FO3Plugin.lnk")

            ' FalloutNV Tools
            AddLaunchTool(ini, "FalloutNV", "Wrye Flash", "Mopy\Wrye Bash Launcher.pyw")
            AddLaunchTool(ini, "FalloutNV", "FOMM", "fomm\fomm.exe")
            AddLaunchTool(ini, "FalloutNV", "FalloutNV", "FalloutNV.exe")
            AddLaunchTool(ini, "FalloutNV", "FalloutNV Launcher", "FalloutNVLauncher.exe")
            AddLaunchTool(ini, "FalloutNV", "GECK", "Geck.exe")
            AddLaunchTool(ini, "FalloutNV", "FO3Edit", "FO3Edit.exe")
            AddLaunchTool(ini, "FalloutNV", "FO3Plugin", "FO3Plugin.jar", "-Xmx1024m", "FO3Plugin.lnk")

            ' Custom Tools
            AddLaunchTool(ini, "Custom", "TES4Gecko", "TES4Gecko.jar", "-Xmx1024m")
            AddLaunchTool(ini, "Custom", "TES4Files", "Tools\TES4Files.exe")
            AddLaunchTool(ini, "Custom", "TES4LodGen", "TES4LodGen.exe")

            ' Finished
            ini_save(Get_AppPath("mTES4.ini"), ini)
        End If

        game = Get_TrackGame()
        If cbGames.Items.Contains(game) Then
            cbGames.SelectedIndex = cbGames.Items.IndexOf(game)
        ElseIf cbGames.Items.Count Then
            cbGames.SelectedIndex = 0
        Else ' no detected games?
        End If
    End Sub

    Private Sub Manage_Game()
        Dim dirinfo As DirectoryInfo
        Dim game As String
        Dim index As Integer

        game = Set_TrackGame(cbGames.SelectedItem)
        dirinfo = My.Computer.FileSystem.GetDirectoryInfo(RegGet_InstallPath(game))
        GamePath = dirinfo.Parent.FullName
        InstallPath = dirinfo.Name

        mTES4_Game = game & "-mTES4"

        TrackActive = CheckActive(InstallPath)
        If TrackActive.Equals("") Then
            If cbGames.Items.Count > 1 Then
                index = cbGames.SelectedIndex + 1
                If index >= cbGames.Items.Count Then index = 0
                cbGames.SelectedIndex = index
            Else
                Me.Close()
            End If
            Exit Sub
        End If

        'Populate the detected clones list
        PopulateClones(lbClones, InstallPath)
        If lbClones.Items.Count Then lbClones.SelectedIndex = 0
    End Sub

    Private Sub cbGames_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbGames.SelectedIndexChanged
        Manage_Game()
    End Sub

    Private Sub lbClones_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbClones.SelectedIndexChanged
        Dim index As Integer
        index = lbClones.SelectedIndex
        btnSwitch.Enabled = (index > 0)
        btnDelete.Enabled = (index > 0)
        btnLaunch.Enabled = (index = 0)
    End Sub

    Private Sub lbClones_DrawItem(ByVal sender As Object, ByVal e As System.Windows.Forms.DrawItemEventArgs) Handles lbClones.DrawItem
        Dim itemfont As Font
        Dim itemstyle As FontStyle
        Dim itembrush As Brush

        If e.Index = -1 Then Exit Sub

        lbClones.DrawMode = DrawMode.OwnerDrawFixed
        e.DrawBackground()

        If e.Index = 0 Then ' Active Clone
            itemstyle = FontStyle.Bold
        Else ' Inactive clone
            itemstyle = FontStyle.Regular
        End If
        itemfont = New Font(e.Font.Name, e.Font.Size, itemstyle)
        itembrush = New SolidBrush(e.ForeColor)

        e.Graphics.DrawString(lbClones.Items(e.Index), itemfont, itembrush, New RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height))
        ' If the ListBox has focus, draw a focus rectangle around the selected item.
        e.DrawFocusRectangle()
    End Sub

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Dim imported As Boolean
        imported = ImportClone()
        If imported Then PopulateClones(lbClones, InstallPath)
    End Sub

    Private Sub btnClone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClone.Click
        Dim copied As Boolean
        copied = CopyClone()
        If copied Then PopulateClones(lbClones, InstallPath)
    End Sub

    Private Sub btnSwitch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSwitch.Click
        doSwitch()
    End Sub

    Private Sub doSwitch()
        Dim ini As String()
        Dim title, launch As String
        Dim dirinfo As DirectoryInfo

        If SwitchClone(lbClones.SelectedItem) Then
            PopulateClones(lbClones, InstallPath)

            ini = ini_load(Get_GamePath(InstallPath & "\_mTES4 (" & TrackActive & ").ini"))
            launch = ini_get(ini, "AutoLaunch", "sFilePath", "")
            If Not String.Equals(launch, "") Then
                title = ini_get(ini, "AutoLaunch", "sTitle", "")
                If title.Equals("") Then
                    dirinfo = My.Computer.FileSystem.GetDirectoryInfo(launch)
                    title = dirinfo.Name
                End If
                dlgAutoLaunch.lblText.Text = "A custom tool is set to autolaunch for this clone." & vbCrLf _
                                       & "Do you want to launch " & title & " now?"
                dlgAutoLaunch.ShowDialog()
            End If
        End If
    End Sub

    Private Sub btnRename_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRename.Click
        Dim renamed As Boolean
        renamed = RenameClone()
        If renamed Then PopulateClones(lbClones, InstallPath)
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim deleted As Boolean
        deleted = DeleteClone()
        If deleted Then PopulateClones(lbClones, InstallPath)
    End Sub

    Private Sub Main_SizeChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.SizeChanged
        If Not NotifyIcon1.Visible And (Me.WindowState = FormWindowState.Minimized) Then
            NotifyIcon1.Visible = True
            Me.Hide()
        End If
    End Sub

    Private Sub NotifyIcon1_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NotifyIcon1.DoubleClick
        NotifyIcon1.Visible = False
        Me.Show()
        Me.WindowState = FormWindowState.Normal
    End Sub

    Private Sub TrayMenu_Click(ByVal sender As MenuItem, ByVal e As System.EventArgs)
        If sender.Text.Equals("&Open") Then
            NotifyIcon1.Visible = False
            Me.Show()
            Me.WindowState = FormWindowState.Normal
        ElseIf sender.Text.Equals("E&xit") Then
            NotifyIcon1.Visible = False
            Me.Close()
        End If
    End Sub

    Private Sub TrayMenuGames_Popup(ByVal menu As MenuItem, ByVal e As System.EventArgs)
        menu.MenuItems.Clear()
        For i As Integer = 0 To cbGames.Items.Count - 1
            menu.MenuItems.Add(cbGames.Items.Item(i), AddressOf TrayMenuGames_Click)
            If i = 0 Then
                menu.MenuItems.Item(i).Checked = True
                menu.MenuItems.Item(i).Enabled = False
            End If
        Next
    End Sub

    Private Sub TrayMenuGames_Click(ByVal menu As MenuItem, ByVal e As System.EventArgs)
        cbGames.SelectedIndex = cbGames.Items.IndexOf(menu.Text)
    End Sub

    Private Sub TrayMenuClones_Popup(ByVal menu As MenuItem, ByVal e As System.EventArgs)
        menu.MenuItems.Clear()
        For i As Integer = 0 To lbClones.Items.Count - 1
            menu.MenuItems.Add(lbClones.Items.Item(i), AddressOf TrayMenuClones_Click)
            If i = 0 Then
                menu.MenuItems.Item(i).Checked = True
                menu.MenuItems.Item(i).Enabled = False
            End If
        Next
    End Sub

    Private Sub TrayMenuClones_Click(ByVal menu As MenuItem, ByVal e As System.EventArgs)
        lbClones.SelectedIndex = lbClones.Items.IndexOf(menu.Text)
        doSwitch()
    End Sub

    Private Sub btnLaunch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLaunch.Click
        dlgLaunch.ShowDialog()
    End Sub

    Private Sub btnSettings_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSettings.Click
        dlgSettings.ShowDialog()
    End Sub
End Class
