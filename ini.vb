﻿Imports System.IO

Module INI
    Public Function get_comment(ByVal line As String) As String
        Dim f As Integer
        f = line.IndexOf(";")
        While (f > 0) And line.StartsWith(" ") 'keep preceeding whitespace with comment
            f -= 1
        End While
        If (f >= 0) Then
            get_comment = line.Substring(f)
        Else
            get_comment = ""
        End If
    End Function

    Public Function strip_comment(ByVal line As String) As String
        Dim f As Integer
        f = line.IndexOf(";")
        If (f >= 0) Then
            strip_comment = line.Substring(0, f)
        Else
            strip_comment = line
        End If
    End Function

    Public Function is_blank(ByVal line As String) As Boolean
        is_blank = (line.Length() = 0)
    End Function

    Public Function is_section(ByVal line As String) As Boolean
        is_section = False
        If Not is_blank(line) Then
            is_section = line.StartsWith("[")
        End If
    End Function

    Public Function ini_parse_section(ByVal line As String) As String
        Dim f As Integer
        f = line.IndexOf("]")
        If f = -1 Then f = line.Length()
        line = line.Substring(1, f - 1)
        ini_parse_section = line.Trim()
    End Function

    Public Function ini_parse_item_name(ByVal line As String) As String
        Dim f As Integer
        ini_parse_item_name = ""
        f = line.IndexOf("=")
        If (f >= 0) Then
            line = line.Substring(0, f)
            ini_parse_item_name = line.Trim()
        End If
    End Function

    Public Function ini_parse_item_value(ByVal line As String) As String
        Dim f As Integer
        ini_parse_item_value = ""
        f = line.IndexOf("=")
        If (f >= 0) Then
            line = line.Substring(f + 1)
            ini_parse_item_value = line.Trim()
        End If
    End Function

    Public Function ini_load(ByVal src As String) As String()
        Dim ini As String()
        If File.Exists(src) Then
            ini = File.ReadAllLines(src)
            For i As Integer = ini.GetLowerBound(0) To ini.GetUpperBound(0)
                ini.SetValue(ini.GetValue(i).Trim(), i)
            Next
        Else
            ini = Array.CreateInstance(GetType(String), 0)
        End If
        ini_load = ini
    End Function

    Public Sub ini_save(ByVal dst As String, ByVal ini As String())
        File.WriteAllLines(dst, ini)
    End Sub

    Public Function ini_find(ByVal iniSrc As String(), ByVal section As String, ByVal item As String, Optional ByVal offset As Integer = 0) As Integer
        Dim ini As String()
        Dim line As String
        Dim found As Boolean
        ini_find = -1
        ini = Array.CreateInstance(GetType(String), iniSrc.Length)
        Array.Copy(iniSrc, ini, iniSrc.Length)
        section = section.Trim().ToLower()
        item = item.Trim().ToLower()
        If offset < 0 Then offset = 0
        found = False
        For i As Integer = ini.GetLowerBound(0) + offset To ini.GetUpperBound(0)
            line = strip_comment(ini.GetValue(i))
            ini.SetValue(line, i)
            If is_blank(line) Then ' skip blank lines
            ElseIf is_section(line) Then ' section
                If found Then Exit Function ' bail out if we have reached another section
                found = (section.Equals("") Or section.Equals(ini_parse_section(line).ToLower())) ' did we find the section we are looking for?
                If found And item.Equals("") Then
                    ini_find = i
                    Exit Function
                End If
            ElseIf found And item.Equals(ini_parse_item_name(line).ToLower()) Then ' item
                ini_find = i
                Exit Function
            End If
        Next
    End Function

    Public Function ini_insert(ByVal ini As String(), ByVal section As String, ByVal item As String, ByVal data As String, Optional ByVal offset As Integer = 0) As String()
        Dim tmp_ini As String()
        Dim line As String
        Dim f As Integer

        f = ini_find(ini, section, "", offset)
        If f = -1 Then ' need to add a new section
            If Not ini.Length() = 0 Then
                Array.Resize(ini, ini.Length() + 1)
                ini.SetValue("", ini.Length() - 1)
            End If
            Array.Resize(ini, ini.Length() + 1)
            ini.SetValue("[" & section.Trim() & "]", ini.Length() - 1)
            Array.Resize(ini, ini.Length() + 1)
            ini.SetValue(item.Trim() & "=" & data.Trim(), ini.Length() - 1)
            ini_insert = ini
            Exit Function
        End If

        ' find the end of the section
        f += 1
        For i As Integer = f To ini.GetUpperBound(0)
            line = ini.GetValue(i)
            If is_section(line) Then Exit For
            f += 1
        Next
        ' back up past any consecutive blank lines and comments
        f -= 1
        For i As Integer = f To ini.GetLowerBound(0) Step -1
            line = ini.GetValue(i)
            If Not is_blank(strip_comment(line)) Then Exit For
            f -= 1
        Next
        f += 1

        ' insert the new item
        tmp_ini = Array.CreateInstance(GetType(String), ini.Length() + 1)
        Array.ConstrainedCopy(ini, 0, tmp_ini, 0, f)
        tmp_ini.SetValue(item.Trim() & "=" & data.Trim(), f)
        Array.ConstrainedCopy(ini, f, tmp_ini, f + 1, ini.Length() - f)

        ini_insert = tmp_ini
    End Function

    Public Function ini_delete(ByVal ini As String(), ByVal section As String, ByVal item As String, Optional ByVal offset As Integer = 0) As String()
        Dim tmp_ini As String()
        Dim f As Integer
        f = ini_find(ini, section, item, offset)
        If f = -1 Then
            ini_delete = ini
            Exit Function
        End If
        tmp_ini = Array.CreateInstance(GetType(String), ini.Length() - 1)
        Array.ConstrainedCopy(ini, 0, tmp_ini, 0, f - 1)
        Array.ConstrainedCopy(ini, f + 1, tmp_ini, f, ini.Length() - f - 1)
        ini_delete = tmp_ini
    End Function


    Public Function ini_get(ByVal ini As String(), ByVal section As String, ByVal item As String, ByVal def As String, Optional ByVal offset As Integer = 0) As String
        Dim f As Integer
        f = ini_find(ini, section, item, offset)
        If f = -1 Then
            ini_get = def
        Else
            ini_get = ini_parse_item_value(strip_comment(ini.GetValue(f)))
        End If
    End Function

    Public Function ini_set(ByVal ini As String(), ByVal section As String, ByVal item As String, ByVal data As String, Optional ByVal offset As Integer = 0) As String()
        Dim f As Integer
        f = ini_find(ini, section, item, offset)
        If f = -1 Then
            ini_set = ini_insert(ini, section, item, data, offset)
        Else
            ini.SetValue(item & "=" & data & get_comment(ini.GetValue(f)), f)
            ini_set = ini
        End If
    End Function
End Module
