﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main))
        Me.ButtonNext = New System.Windows.Forms.Button
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnImport = New System.Windows.Forms.Button
        Me.btnRename = New System.Windows.Forms.Button
        Me.lbClones = New System.Windows.Forms.ListBox
        Me.btnSwitch = New System.Windows.Forms.Button
        Me.btnDelete = New System.Windows.Forms.Button
        Me.btnClone = New System.Windows.Forms.Button
        Me.cbGames = New System.Windows.Forms.ComboBox
        Me.btnLaunch = New System.Windows.Forms.Button
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.btnSettings = New System.Windows.Forms.Button
        Me.NotifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.cbTray = New System.Windows.Forms.CheckBox
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'ButtonNext
        '
        Me.ButtonNext.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.ButtonNext.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.ButtonNext.Location = New System.Drawing.Point(368, 9)
        Me.ButtonNext.Name = "ButtonNext"
        Me.ButtonNext.Size = New System.Drawing.Size(67, 23)
        Me.ButtonNext.TabIndex = 0
        Me.ButtonNext.Text = "&Next"
        '
        'btnImport
        '
        Me.btnImport.Location = New System.Drawing.Point(6, 125)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(80, 23)
        Me.btnImport.TabIndex = 4
        Me.btnImport.Text = "&Import Clone"
        Me.ToolTip1.SetToolTip(Me.btnImport, "Import a new clone from an existing folder.")
        Me.btnImport.UseVisualStyleBackColor = True
        '
        'btnRename
        '
        Me.btnRename.Location = New System.Drawing.Point(6, 188)
        Me.btnRename.Name = "btnRename"
        Me.btnRename.Size = New System.Drawing.Size(80, 23)
        Me.btnRename.TabIndex = 6
        Me.btnRename.Text = "&Rename"
        Me.ToolTip1.SetToolTip(Me.btnRename, "Rename and retag the selecetd clone.")
        Me.btnRename.UseVisualStyleBackColor = True
        '
        'lbClones
        '
        Me.lbClones.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.lbClones.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbClones.FormattingEnabled = True
        Me.lbClones.ItemHeight = 18
        Me.lbClones.Location = New System.Drawing.Point(92, 16)
        Me.lbClones.Name = "lbClones"
        Me.lbClones.Size = New System.Drawing.Size(172, 220)
        Me.lbClones.TabIndex = 0
        Me.ToolTip1.SetToolTip(Me.lbClones, resources.GetString("lbClones.ToolTip"))
        '
        'btnSwitch
        '
        Me.btnSwitch.Location = New System.Drawing.Point(6, 16)
        Me.btnSwitch.Name = "btnSwitch"
        Me.btnSwitch.Size = New System.Drawing.Size(80, 23)
        Me.btnSwitch.TabIndex = 1
        Me.btnSwitch.Text = "S&witch"
        Me.ToolTip1.SetToolTip(Me.btnSwitch, "Switch the active copy of oblivion to the selected clone.")
        Me.btnSwitch.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(6, 213)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(80, 23)
        Me.btnDelete.TabIndex = 7
        Me.btnDelete.Text = "&Delete"
        Me.ToolTip1.SetToolTip(Me.btnDelete, "Delete the selected clone.")
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnClone
        '
        Me.btnClone.Location = New System.Drawing.Point(6, 150)
        Me.btnClone.Name = "btnClone"
        Me.btnClone.Size = New System.Drawing.Size(80, 23)
        Me.btnClone.TabIndex = 5
        Me.btnClone.Text = "&Copy Clone"
        Me.ToolTip1.SetToolTip(Me.btnClone, "Create a new clone copy from the selected clone.")
        Me.btnClone.UseVisualStyleBackColor = True
        '
        'cbGames
        '
        Me.cbGames.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbGames.FormattingEnabled = True
        Me.cbGames.Items.AddRange(New Object() {"Oblivion", "Fallout3"})
        Me.cbGames.Location = New System.Drawing.Point(6, 16)
        Me.cbGames.Name = "cbGames"
        Me.cbGames.Size = New System.Drawing.Size(258, 21)
        Me.cbGames.TabIndex = 0
        Me.ToolTip1.SetToolTip(Me.cbGames, "List of detected games that mTES4 supports.")
        '
        'btnLaunch
        '
        Me.btnLaunch.Location = New System.Drawing.Point(6, 45)
        Me.btnLaunch.Name = "btnLaunch"
        Me.btnLaunch.Size = New System.Drawing.Size(80, 23)
        Me.btnLaunch.TabIndex = 2
        Me.btnLaunch.Text = "&Launch"
        Me.ToolTip1.SetToolTip(Me.btnLaunch, "Open Launch Tools window for active clone.")
        Me.btnLaunch.UseVisualStyleBackColor = True
        '
        'FolderBrowserDialog1
        '
        Me.FolderBrowserDialog1.ShowNewFolderButton = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cbGames)
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GroupBox1.Location = New System.Drawing.Point(12, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(270, 44)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Detected Games"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnSettings)
        Me.GroupBox2.Controls.Add(Me.btnLaunch)
        Me.GroupBox2.Controls.Add(Me.lbClones)
        Me.GroupBox2.Controls.Add(Me.btnDelete)
        Me.GroupBox2.Controls.Add(Me.btnImport)
        Me.GroupBox2.Controls.Add(Me.btnClone)
        Me.GroupBox2.Controls.Add(Me.btnRename)
        Me.GroupBox2.Controls.Add(Me.btnSwitch)
        Me.GroupBox2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GroupBox2.Location = New System.Drawing.Point(12, 56)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(270, 242)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Detected Clones"
        '
        'btnSettings
        '
        Me.btnSettings.Location = New System.Drawing.Point(6, 74)
        Me.btnSettings.Name = "btnSettings"
        Me.btnSettings.Size = New System.Drawing.Size(80, 23)
        Me.btnSettings.TabIndex = 3
        Me.btnSettings.Text = "&Settings"
        Me.ToolTip1.SetToolTip(Me.btnSettings, "Customize settings for the selected clone.")
        Me.btnSettings.UseVisualStyleBackColor = True
        '
        'NotifyIcon1
        '
        Me.NotifyIcon1.BalloonTipTitle = "mTES4 Manager"
        Me.NotifyIcon1.Icon = CType(resources.GetObject("NotifyIcon1.Icon"), System.Drawing.Icon)
        Me.NotifyIcon1.Text = "mTES4 Manager"
        '
        'cbTray
        '
        Me.cbTray.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbTray.AutoSize = True
        Me.cbTray.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cbTray.Checked = True
        Me.cbTray.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbTray.Location = New System.Drawing.Point(143, 302)
        Me.cbTray.Name = "cbTray"
        Me.cbTray.Size = New System.Drawing.Size(139, 17)
        Me.cbTray.TabIndex = 2
        Me.cbTray.Text = "Minimize to System Tray"
        Me.ToolTip1.SetToolTip(Me.cbTray, resources.GetString("cbTray.ToolTip"))
        Me.cbTray.UseVisualStyleBackColor = True
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(294, 321)
        Me.Controls.Add(Me.cbTray)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "Main"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "mTES4 Manager"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ButtonNext As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents btnImport As System.Windows.Forms.Button
    Friend WithEvents btnRename As System.Windows.Forms.Button
    Friend WithEvents lbClones As System.Windows.Forms.ListBox
    Friend WithEvents btnSwitch As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnClone As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cbGames As System.Windows.Forms.ComboBox
    Friend WithEvents NotifyIcon1 As System.Windows.Forms.NotifyIcon
    Friend WithEvents cbTray As System.Windows.Forms.CheckBox
    Friend WithEvents btnLaunch As System.Windows.Forms.Button
    Friend WithEvents btnSettings As System.Windows.Forms.Button

End Class
