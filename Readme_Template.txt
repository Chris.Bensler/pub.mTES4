Official Download Page: [url=http://www.tesnexus.com/downloads/file.php?id=35263]TESNexus[/url]
Direct Downloads:
  Stable Version:  [url=http://www.tesnexus.com/downloads/download.php?id=71043]mTES4 v0.5.1b rev76[/url]
  Latest Version:  [url=http://www.tesnexus.com/downloads/download.php?id=71044]mTES4 v0.6.1b rev93[/url]
  
Latest Thread: [url=http://forums.bethsoft.com/index.php?/topic/1142151-rel-mtes4-manager/][WIPz] mTES4 Manager[/url]
Previous Thread: [url=http://forums.bethsoft.com/index.php?/topic/1119357-wipz-mtes4-manager/][WIPz] mTES4 Manager[/url]

[size=2][color=#FF0000][i]For help getting started, read...[/i][/color]
[url=http://www.tesnexus.com/articles/article.php?id=432][color=#FF0000][b]A Guide to Setting Up Multiple Copies of Oblivion using mTES4[/b][/color][/url][/size].
I suggest everyone reads the guide. There is some very useful advice concerning best practices that will benefit you regardless if you need orientation.

-------------------------------------------------------------------------------
[size=3][b][i](Not just)[/i] Oblivion has multiplied![/b][/size]
mTES4 Manager lets you create complete copies of your game and helps you to manage them.

Supported Games:
- Oblivion
- Fallout3
- [color=#DDDD00][i]*NEW* Fallout:New Vegas[/i][/color]
[i]Support for Morrowind coming soon.[/i]

If you are a modder, this means you can have a completely separate copy of the game just for development and easily switch between your play and mod copies.
If you are not a modder, you can have a separate copy of the game just for trying new mods, tools and total conversions!

Making a mod and you need to test against multiple versions of the game or another mod? No problem.
Playing FCOM and you want to try out TIE by itself but you don't want to lose your game? No sweat.
Want to play Nehrim, but also want to keep your current Oblivion game? Couldn't be easier.

Never again do you need to reinstall your whole game because you tried to add something that just didn't work out as you planned.

-------------------------------------------------------------------------------
Overview:

[color=#FF0000][i]Please note that this documentation generally refers to Oblivion, but it is the same for all of the supported games unless stated otherwise.
Just replace references to 'Oblivion' with 'Fallout3' or 'FalloutNV'.[/i][/color]

mTES4 Manager is a tool that helps you to create and manage multiple copies of Oblivion.
Essentially, it takes care of swapping the game, profile and appdata folders for you so that every
instance of Oblivion you have installed will have it's own unique working environment.

You can import existing game folders as new clones, copy clones, rename or delete and of course switch between them.

[color=#DDDD00][i]*NEW* mTES4 now supports Fallout:New Vegas in addition to Oblivion and Fallout3![/i][/color]
It will automatically detect if you have any of the supported games installed and provide you the option to switch between managing
each of them from the GUI.

Nehrim will also work, but it is not considered a supported game, it is considered a copy of oblivion.
So you can just import your Nehrim copy to your Oblivion clones in mTES4 and it will be treated the same as any other.
[color=#DDDD00][i]*NEW* You can now import clones from any folder or drive! You do not have to move your Nehrim folder any more.[/i][/color]
[s](You will need to move your Nehrim folder from Bethesda Softwork\SureAI\Nehrim\ to Bethesda Softworks\Nehrim to be able to import it.)[/s]

mTES4 also supports Wrye Bash.
If you have Bash installed in the clone you are copying from, each time you create a clone, mTES4 will ask you to select the BAIN mods folder
that will be used for the new clone. [s]You can choose to use the same BAIN Mods folder for every clone, or you can have a
separate folder for each. It's up to you.[/s] There is currently some issues with Wrye bash if you try to use the same Oblivion Mods folder for multiple clones.
For now, it's strongly advised to use a separate Oblivion Mods folder for each clone.

Most all other 3rd party tools should work without issues.
If you find a tool that you do have problems using in conjunction with installation swapping, please report it and I'll do
my best to ensure that it is addressed.

-------------------------------------------------------------------------------
Installing:

There is no formal installation required. Just unzip and run mTES4.exe

When you first run mTES4, it will automatically detect your installed game and tag it with a unique name (that you provide).
Once your game has been tagged, mTES4 will recognize it as a valid copy of Oblivion (what mTES4 refers to as a 'clone').

If you have multiple copies of Oblivion already, you can import them so that mTES4 will detect them.
You can also create new clones by copying an existing clone.

Once you have setup your clones, you can use mTES4 to switch between them at the click of a button.

-------------------------------------------------------------------------------
Uninstalling:

Just delete the mTES4 folder.

mTES4 also creates two special folders, which contain the savegames and game settings for all the clones.
You can delete them as well if you don't want to save them.

(The following paths are defaults for Windows XP)
mTES4's clone profile folder is at C:\Documents and Settings\USER\My Documents\My Games\Oblivion-mTES4
mTES4's clone appdata folder is at C:\Documents and Settings\USER\Local Settings\Application Data\Oblivion-mTES4


-------------------------------------------------------------------------------
Usage:

Importing a New Clone:
If you have an existing copy of Oblivion installed that has not been configured for mTES4 already, you can import it so that it will be detected.
When you import a new clone, mTES4 will automatically create a separate profile and appdata folder for it (see the sections below about Oblivion and mTES4 Folders).
The profile and appdata folders will be empty so if you have existing savegames, an oblivion.ini, bash settings, etc
and/or plugins lists for your existing copy, you will want to manually copy or move those files into the appropriate folders.
Otherwise, Oblivion will automatically create what it needs.

Copying Clones:
If you have already setup a clone for mTES4, you can copy it to create a new clone.
The profile and appdata folders will be copied for you as well so both clones will be identical copies.

Renaming a Clone:
Because the oblivion game is spread over multiple places, mTES4 provides a way for you to rename a clone if you wish.
All relevant file and folder names will be updated for you.

Deleting a Clone:
Like renaming, if you wish to delete a clone, mTES4 provides a way for you to do so.
All relevant files and folders will be deleted if you choose.

Switching Between Clones:
When you wish to change to a different copy of Oblivion, you can simply select it from the Detected Clones List then click the Switch button.
mTES4 takes care of the rest.

Minimize To System Tray:
Enabling this option will hide mTES4 so you will not see it on your taskbar.
Instead the mTES4 icon will appear in your system tray.
When mTES4 is minimized to the tray, you can point your mouse at the icon for a description of the active game and clone.
You can also right-click the icon for a menu that will allow you to switch games and clones even more conveniently!


-------------------------------------------------------------------------------
mTES4 AutoLaunch:

When you switch to a new clone, if an autolaunch setting is defined, mTES4 will ask you if you want to launch the configured tool.
You can define autolaunch settings for each clone.

To configure the autolaunch settings, select the clone you want and click the Settings button.
You can specify the Title that will be displayed, the path to execute and any arguments that might be needed.
If you enable Always Launch, the AutoLaunch tool will be executed immediately after switching without asking you.


-------------------------------------------------------------------------------
Launch Tools:

When you click the launch button, you will be presented with a list of tools to execute that you can choose from.
Double-click any tool, mTES4 will be minimized and the selected tool will be launched.

By default, mTES4 will try to detect some of the most common tools and offer them to you.
You can however customize the launch tools to your preference, either adding, removing, or reordering them.

OBSE and FOSE are special cases and cannot be customized.


-------------------------------------------------------------------------------
Customizing the Launch Tools:

There are a few slightly different ways you can add new tools so they will be presented to you in the Launch dialog.
1. Common tools can be defined that will be available with any game and with any clone selected.
2. Game tools can be defined that will be available with only a specific game, but with any clone selected
3. Clone tools can be defined that will only be available for a specific clone.

Each tool has it's own section in the ini file.
The name of the section depends on how you want the tool to be available.

In the clone tag file, you can only define one type of tool.
The tools defined in a clone's tag file will only be available when that clone is active.
Every custom tool in the tag file will have the section name of [Custom Tool].
You can define multiple custom tools by having multiple [Custom Tool] sections with different settings.

In the mTES4.ini file, you can define two other types of tools.
The tools defined in the mTES4.ini file will either be available to all clones for a specific game, or all clones for all games.

To define a tool for a specific game, the tools section name will be [%GAME NAME% Tool].
EG. A tool that is only available when Oblivion is active will be in a section named [Oblivion Tool].

In mTES4.ini, the [Custom Tool] section is used to define a tool that is always available, 
regardless of what game or clone is active.


The settings for any custom tool are:
sFilePath ; Required, The path to the executable. This can be relative to the game folder or it can be a full path.
sArguments ; Optional, If any extra arguments are needed, you can define them here.
sIconPath ; Optional, The path to the icon that should be displayed in the launch dialog for this tool.
sTitle ; Optional, The name of the tool displayed in the launch dialog.

If sFilePath or sIconPath is relative, it should not begin with a \
If no icon is configured, mTES4 will try to extract the icon from sFilePath.
If no title is defined, mTES4 will use the file name.


-------------------------------------------------------------------------------
mTES4 Folders:
(NOTE: the paths used for Oblivion, Fallout3 and Fallout:New Vegas are nearly identical. Just replace all references to Oblivion with Fallout3 or FalloutNV)

As you may or may not already know, Oblivion has files in 3 different places that mTES4 keeps track of for you.
1. The game folder
This is the folder where your game is installed (contains Oblivion.exe)
By default, on winXP this folder is "C:\Program Files\Bethesda Softworks\Oblivion"

2. The profile folder
This is the folder where your savegames and oblivion.ini are.
By default, on winXP this folder is "C:\Documents and Settings\[i]USER[/i]\My Documents\My Games\Oblivion"

3. The appdata folder
This is the folder where oblivion keeps the list of plugins that are installed.
By default, on winXP this is "C:\Documents and Settings\[i]USER[/i]\Local Settings\Application Data\Oblivion"

mTES4 creates two additional folders to store the profiles and appdata for your clones.
4. The clone profile folder
This is the folder where mTES4 will store the profile folders for your inactive clones.
When you switch clones, mTES4 will swap your active profile folder with the appropriate folder stored in the clone profile folder
By default, on winXP this folder is "C:\Documents and Settings\[i]USER[/i]\My Documents\My Games\Oblivion-mTES4"
Eg. Assuming your active copy is named Oblivion1 and you have a 2nd clone named Oblivion2
The profile folder for Oblivion1 will be "...\My Games\Oblivion"                  (it uses the real oblivion folder)
Because Oblivion2 is an inactive clone, the profile folder will be "...\My Games\Oblivion-mTES4\Oblivion2"    (it is stored in the mTES4 folder using its actual clone name)

5. The clone appdata folder
This is the folder where mTES4 will store the appdata folders for your inactive clones.
When you switch clones, mTES4 will swap your active appdata folder with the appropriate folder stored in the clone appdata folder.
By default, on winXP this is "C:\Documents and Settings\[i]USER[/i]\Local Settings\Application Data\Oblivion-mTES4"
See the example for The clone profile folder. The clone appdata folder works similarly.

[b]For instance, if your windows XP login name is Joe and you have both Oblivion and Fallout3 installed in their default locations, you will have ALL of these folders:[/b]

[i]C:\Program Files\Bethesda Softworks\Fallout3[/i]
[i]C:\Program Files\Bethesda Softworks\Oblivion[/i]

[i]C:\Documents and Settings\Joe\My Documents\My Games\Fallout3[/i]
[i]C:\Documents and Settings\Joe\My Documents\My Games\Oblivion[/i]

[i]C:\Documents and Settings\Joe\Local Settings\Application Data\Fallout3[/i]
[i]C:\Documents and Settings\Joe\Local Settings\Application Data\Oblivion[/i]

[b]mTES4 adds the following folders:[/b]

[i]C:\Documents and Settings\Joe\My Documents\My Games\Fallout3-mTES4[/i]
[i]C:\Documents and Settings\Joe\My Documents\My Games\Oblivion-mTES4[/i]

[i]C:\Documents and Settings\Joe\Local Settings\Application Data\Fallout3-mTES4[/i]
[i]C:\Documents and Settings\Joe\Local Settings\Application Data\Oblivion-mTES4[/i]


-------------------------------------------------------------------------------
Appendix:

Clones:
A clone is what mTES4 calls any copy of your game that has been tagged as a valid copy of Oblivion.

Tags:
A tag is a specially name ini file that is placed inside the game folder of each clone.
A tag file looks like this: "_mTES4 (NAME OF CLONE).ini"
Part of the purpose of these tags is simply so that you can easily see which copy you have currently active from windows explorer without having to run mTES4.
The tags are also used to determine which folders to display in the mTES4 Detected Clones List.
You can also define custom tools for mTES4 launch dialog which will only be displayed for this clone.

mTES4.ini:
This file is used to keep track of which game and clone you have selected.
You can find the mTES4.ini file in the same folder as mTES4.exe.
You can also define custom tools for mTES4 launch dialog that will be displayed for all games and clones or just for the clones of a specific game.


-------------------------------------------------------------------------------
Planned Features:
- [v0.1] Clone new copies of Oblivion
- [v0.1] Switch between clone copies of Oblivion
- [v0.2] Import existing game copies
- [v0.2] Rename clones
- [v0.2] Delete clones
- [v0.2] Nehrim Support
- [v0.3] Fallout3 Support
- [v0.4] Remove requirement for mTES4.exe to be in any particular folder
- [v0.4] Improve Delete functionality to provide checkbox options for game, profile/appdata and/or just tag
- [v0.4] Support for Wrye Flash
- [v0.5] 3rd party app launch buttons
- [v0.5] Per-clone auto launch options
- [v0.5] Option to minimize to system tray
- [v0.5] System tray r-click menu
- [v0.6] [color=#DDDD00][i]*NEW* Support for Fallout:New Vegas[/i][/color]
- [v0.6] [color=#DDDD00][i]*NEW* Allow users to import profile and appdata folders as well as game folders[/i][/color]
- [v0.6] [color=#DDDD00][i]*NEW* Import from alternate locations[/i][/color]
- [v0.X] Support for Morrowind

Considerations:
- Function to open folder(s) for the selected clone
- Complete functionality from system tray context menu
- Customize tools from the GUI
- Make windows resizable
- Optimize storage of inactive clones
 
-------------------------------------------------------------------------------
Known Bugs/Workarounds:

-------------------------------------------------------------------------------
Credits: 
Psymon: For feedback and assistance, bug reports and specifics for nehrim and fallout3 support


-------------------------------------------------------------------------------
Change Log:

----------------------------------------
Version 0.6.1b
- added a missing application config file.

----------------------------------------
Version 0.6.0b
- added: Support for Fallout:New Vegas
- added: ability to import clones from alternate locations
- added: ability to copy profile and appdata folders when cloning or importing
- bug fix: tray icon hint text updates incorrectly if switching clones fails
- bug fix: Launch dialog does not remember the list view preference after closing
- bug fix: incorrect hint text for Launch button
- bug fix: incorrect (obsolete) hint text for Detected Clones list
- bug fix: missing hint text for Settings button
- bug fix: missing hint text for Minimize to System Tray option
- bug fix: missing hint text for Launch dialog
- bug fix: missing hint text for Settings dialog

----------------------------------------
Version 0.5.1b
- bug fix for importing, copying or renaming clones fails in v0.5b

----------------------------------------
Version 0.5.0b
- New minimize to tray option. Hovering over the tray icon will show the currently active game and clone.
- New r-click menu for the tray icon allows you to conveniently switch games or clones.
- New custom autolaunch tools settings for each clone.
- New Launch Tools dialog. Allows you to choose what tool you want to launch.
- New Custom Tool settings can be configured in mTES4.ini and the _mTES4 (*).ini tag files.
- bug fix for possible duplicate named clones if you have more than one supported game installed.
Affected Import, Copy and Rename features

----------------------------------------
Version 0.4.0a
- Support for Wrye Flash
- Added ability to use existing profile and appdata folders when importing.
- New Delete Clone dialog, provides options to keep or delete various clone files and folders.
- Removed requirement for mTES4.exe to be in any particular folder
- 'New Clone' button renamed to 'Copy Clone' to better reflect it's functionality.
- bug fix for attempting to rename the active clone crashes mTES4.
- bug fix for incorrectly assuming that if Bash is installed then bash.ini must exist.
  Will now create a new bash.ini from the bash_default.ini if it needs to.
- bug fix for incorrectly parsing bash.ini
- bug fix for no warning when importing or copying a clone, bash is detected but you cancel the BAIN folder selection.
- bug fix for falsely detecting supported games when only placeholder registry keys exist

----------------------------------------
Version 0.3.3a
- reverted fix for querying fallout reg key.
- bug fix for potentially creating two clones with identical names.
- improved the behaviour of popup msgboxes.

----------------------------------------
Version 0.3.2a
- bug fix for mTES4 querying the wrong registry key path on x64 systems.
- bug fix for querying fallout reg key using "Fallout3" instead of "Fallout 3".

----------------------------------------
Version 0.3.1a
- bug fix for mTES4 not running on Vista and Windows 7.

----------------------------------------
Version 0.3.0a
Should be backward compatible with v0.2
- Integrated support for Fallout3.
- New ini and tag settings to track supported Bethesda games.
- Various minor cosmetic and non-critical bug fixes.

----------------------------------------
Version 0.2.0a
NOTE: v0.2 is NOT backward compatible with v0.1!
- Eliminated the use of the MasterCopy clone. mTES4 is much simpler to use now.
- Added ability to copy from any existing clone.
- Added Import Clone feature.
- Added Rename Clone feature.
- Added Delete Clone feature.
- Moved inactive clone profiles and appdata into mTES4 folders so they don't clutter My Games\ and Application Data\

----------------------------------------
Version 0.1.0a
- Initial alpha release.

----------------------------------------
Terms of Use:
mTES4 is distributed as free software released under a copyleft agreement.
Do whatever you like, just don't sell or otherwise take credit for my work.

Conversely, I do not want credit for your work either.
If you redistribute this software with any modifications whatsoever, you must clearly indicate such and provide the user with access to the original release either accompanied directly with your distribution or by linking to the official download.

Enough said :)