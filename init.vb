﻿Imports System.IO

Module Init
    Public Function IsWow64()
        IsWow64 = (IntPtr.Size = 8)
    End Function

    Public Function Get_AppPath(ByVal name As String) As String
        Get_AppPath = My.Computer.FileSystem.CurrentDirectory
        If Not name.Equals("") Then Get_AppPath = Path.Combine(Get_AppPath, name)
    End Function

    Public Function Get_GamePath(ByVal name As String) As String
        Get_GamePath = Main.GamePath
        If Not name.Equals("") Then Get_GamePath = Path.Combine(Get_GamePath, name)
    End Function

    Public Function Get_ProfilePath(ByVal name As String) As String
        Get_ProfilePath = Path.Combine(My.Computer.FileSystem.SpecialDirectories.MyDocuments, "My Games")
        If Not name.Equals("") Then Get_ProfilePath = Path.Combine(Get_ProfilePath, name)
    End Function

    Public Function Get_AppDataPath(ByVal name As String) As String
        Get_AppDataPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)
        If Not name.Equals("") Then Get_AppDataPath = Path.Combine(Get_AppDataPath, name)
    End Function

    Public Function Get_ProfileClonePath(ByVal name As String) As String
        Get_ProfileClonePath = Path.Combine(Get_ProfilePath(""), Main.mTES4_Game)
        If Not name.Equals("") Then Get_ProfileClonePath = Path.Combine(Get_ProfileClonePath, name)
    End Function

    Public Function Get_AppDataClonePath(ByVal name As String) As String
        Get_AppDataClonePath = Path.Combine(Get_AppDataPath(""), Main.mTES4_Game)
        If Not name.Equals("") Then Get_AppDataClonePath = Path.Combine(Get_AppDataClonePath, name)
    End Function

    Public Function GetParentPath(ByVal src As String) As String
        Dim dirinfo As DirectoryInfo
        dirinfo = My.Computer.FileSystem.GetDirectoryInfo(src)
        GetParentPath = dirinfo.Parent.FullName
    End Function

    Public Function RegGet_InstallPath(ByVal game As String) As String
        Dim key As String
        Try
            key = "HKEY_LOCAL_MACHINE\SOFTWARE\"
            If IsWow64() Then key &= "Wow6432Node\"
            key &= "Bethesda Softworks\" & game
            RegGet_InstallPath = My.Computer.Registry.GetValue(key, "Installed Path", "")
        Catch ex As Exception
            RegGet_InstallPath = ""
        End Try
    End Function

    Public Function Get_TrackGame() As String
        Dim ini() As String

        ini = ini_load(Get_AppPath("mTES4.ini"))
        Get_TrackGame = ini_get(ini, "General", "sMTES4_TrackGame", "")
    End Function

    Public Function Set_TrackGame(ByVal game As String) As String
        Dim ini As String()
        ini = ini_load(Get_AppPath("mTES4.ini"))
        ini = ini_set(ini, "General", "sMTES4_TrackGame", game)
        ini_save(Get_AppPath("mTES4.ini"), ini)
        Set_TrackGame = game
    End Function

    Public Function Get_TrackActive(ByVal game As String) As String
        Dim ini() As String
        ini = ini_load(Get_AppPath("mTES4.ini"))
        Get_TrackActive = ini_get(ini, game, "sMTES4_TrackActive", "")
    End Function

    Public Function Set_TrackActive(ByVal game As String, ByVal name As String) As String
        Dim ini As String()
        ini = ini_load(Get_AppPath("mTES4.ini"))
        ini = ini_set(ini, game, "sMTES4_TrackActive", name)
        ini_save(Get_AppPath("mTES4.ini"), ini)
        Set_TrackActive = name
    End Function

    Public Function CheckActive(ByVal Installpath As String) As String
        Dim ini, files As String()
        Dim dat_file, name As String
        Dim done As Boolean
        Dim active As String

        CheckActive = ""
        name = ""
        active = Get_TrackActive(Main.cbGames.SelectedItem)

        files = Directory.GetFiles(Get_GamePath(Installpath), "_mTES4 (*).ini")
        If files.Length() = 0 Then
            If Not active.Equals("") Then
                If MsgBox("Your active install copy of " & Main.cbGames.SelectedItem & " does not have an mTES4 tracking tag." & vbCrLf _
                        & "The last known active clone was """ & active & """." & vbCrLf _
                        & "Is this correct?" _
                         , MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                    name = active
                End If
            End If

            done = False
            While Not done
                If name.Equals("") Then
                    name = InputBox("Your active install copy of " & Main.cbGames.SelectedItem & " does not have an mTES4 tracking tag." & vbCrLf _
                           & "Please provide a unique name for this install copy." _
                           , "mTES4: Tagging installed copy of " & Main.cbGames.SelectedItem & "." _
                           , "")
                    If name.Equals("") Then Exit Function
                End If

                If Not Directory.Exists(name) Then
                    dat_file = Get_GamePath(Installpath & "\_mTES4 (" & name & ").ini")
                    ini = ini_load(dat_file)
                    ini = ini_set(ini, "General", "sMTES4_Game", Main.cbGames.SelectedItem)
                    ini = ini_set(ini, "General", "sMTES4_Clone", name)
                    ini_save(dat_file, ini)
                    done = True
                Else
                    Error_UniqueName(name, Get_GamePath(""))
                    name = ""
                End If
            End While
        Else
            ini = ini_load(files.GetValue(0))
            name = ini_get(ini, "General", "sMTES4_Clone", "")
        End If
        CheckActive = Get_TrackActive(Main.cbGames.SelectedItem)
        If Not CheckActive.Equals(name) Then
            CheckActive = Set_TrackActive(Main.cbGames.SelectedItem, name)
        End If
    End Function
End Module