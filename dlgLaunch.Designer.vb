﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class dlgLaunch
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(dlgLaunch))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cmMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.miIcons = New System.Windows.Forms.ToolStripMenuItem
        Me.miList = New System.Windows.Forms.ToolStripMenuItem
        Me.ilTools32 = New System.Windows.Forms.ImageList(Me.components)
        Me.ilTools16 = New System.Windows.Forms.ImageList(Me.components)
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnLaunch = New System.Windows.Forms.Button
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lvTools = New System.Windows.Forms.ListView
        Me.GroupBox1.SuspendLayout()
        Me.cmMenu.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lvTools)
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(270, 263)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Detected Tools"
        '
        'cmMenu
        '
        Me.cmMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.miIcons, Me.miList})
        Me.cmMenu.Name = "cmMenu"
        Me.cmMenu.ShowCheckMargin = True
        Me.cmMenu.ShowImageMargin = False
        Me.cmMenu.Size = New System.Drawing.Size(153, 70)
        '
        'miIcons
        '
        Me.miIcons.Checked = True
        Me.miIcons.CheckState = System.Windows.Forms.CheckState.Checked
        Me.miIcons.Name = "miIcons"
        Me.miIcons.Size = New System.Drawing.Size(152, 22)
        Me.miIcons.Text = "Icons"
        '
        'miList
        '
        Me.miList.Name = "miList"
        Me.miList.Size = New System.Drawing.Size(152, 22)
        Me.miList.Text = "List"
        '
        'ilTools32
        '
        Me.ilTools32.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.ilTools32.ImageSize = New System.Drawing.Size(32, 32)
        Me.ilTools32.TransparentColor = System.Drawing.Color.Transparent
        '
        'ilTools16
        '
        Me.ilTools16.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.ilTools16.ImageSize = New System.Drawing.Size(16, 16)
        Me.ilTools16.TransparentColor = System.Drawing.Color.Transparent
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Location = New System.Drawing.Point(12, 286)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "Close"
        Me.ToolTip1.SetToolTip(Me.btnClose, "Cancel and return to the main window.")
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnLaunch
        '
        Me.btnLaunch.Location = New System.Drawing.Point(207, 286)
        Me.btnLaunch.Name = "btnLaunch"
        Me.btnLaunch.Size = New System.Drawing.Size(75, 23)
        Me.btnLaunch.TabIndex = 2
        Me.btnLaunch.Text = "&Launch"
        Me.ToolTip1.SetToolTip(Me.btnLaunch, "Launch the selected tool. (You can just double-click any tool instead of using th" & _
                "is button)")
        Me.btnLaunch.UseVisualStyleBackColor = True
        '
        'lvTools
        '
        Me.lvTools.Activation = System.Windows.Forms.ItemActivation.TwoClick
        Me.lvTools.AutoArrange = False
        Me.lvTools.ContextMenuStrip = Me.cmMenu
        Me.lvTools.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvTools.LargeImageList = Me.ilTools32
        Me.lvTools.Location = New System.Drawing.Point(6, 13)
        Me.lvTools.MultiSelect = False
        Me.lvTools.Name = "lvTools"
        Me.lvTools.ShowGroups = False
        Me.lvTools.Size = New System.Drawing.Size(258, 244)
        Me.lvTools.SmallImageList = Me.ilTools16
        Me.lvTools.StateImageList = Me.ilTools16
        Me.lvTools.TabIndex = 0
        Me.ToolTip1.SetToolTip(Me.lvTools, resources.GetString("lvTools.ToolTip"))
        Me.lvTools.UseCompatibleStateImageBehavior = False
        '
        'dlgLaunch
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(294, 321)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnLaunch)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "dlgLaunch"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "mTES4: LaunchTools"
        Me.GroupBox1.ResumeLayout(False)
        Me.cmMenu.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents lvTools As System.Windows.Forms.ListView
    Friend WithEvents ilTools32 As System.Windows.Forms.ImageList
    Friend WithEvents cmMenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents miIcons As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miList As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ilTools16 As System.Windows.Forms.ImageList
    Friend WithEvents btnLaunch As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip

End Class
