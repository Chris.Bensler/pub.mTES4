@echo off
SETLOCAL

SET VER_MAJOR=0
SET VER_MINOR=6
SET VER_PATCH=1
SET VER_ALPHA=b

SET VERSION=%VER_MAJOR%.%VER_MINOR%.%VER_PATCH%%VER_ALPHA%
SET REVISION=0

:MAIN
  PUSHD %~1
  CALL:GET_REVISION
  CALL:UPDATE_README
  CALL:UPDATE_APPCONFIG
  CALL:UPDATE_ASSEMBLY
  EXIT /B

:GET_REVISION
  REM Get the top-level svn rev
  PUSHD "..\"
  FOR /F "usebackq tokens=1,2* delims=:" %%I IN (`svnversion`) DO IF "%%J" == "" (SET SVN=%%I) else (SET SVN=%%J)
  SET REVISION=%SVN:M=%
  SET /a REVISION=%REVISION%
  POPD
  EXIT /B

:UPDATE_README
REM  replace.vbs "Readme.txt" "Version:.*" "Version:  v%VERSION% rev%REVISION%"
REM  replace.vbs "Readme.txt" "mTES4%%20v\d+\.\d+\.\d+.*%%20r\d+" "mTES4%%20v%VERSION%%%20r%REVISION%"
REM  replace.vbs "Readme.txt" "mTES4 v\d+\.\d+\.\d+.* rev\d+" "mTES4 v%VERSION% rev%REVISION%"
  SET SRC=Readme_Template.txt
  SET DST=Readme.txt
  IF EXIST "%DST%" DEL "%DST%" /Q
  >  "%DST%" ECHO.-------------------------------------------------------------------------------
  >> "%DST%" ECHO.Title:    mTES4 Manager
  >> "%DST%" ECHO.Author:   Gaticus Archimedes Windrider
  >> "%DST%" ECHO.Stable Version:  [url="http://gaticus.iwireweb.com/download/mTES4%%20v0.5.1b%%20r76.zip"]mTES4 v0.5.1b rev76[/url]
  >> "%DST%" ECHO.Latest Version:  [url="http://gaticus.iwireweb.com/download/mTES4%%20v%VERSION%%%20r%REVISION%.zip"]mTES4 v%VERSION% rev%REVISION%[/url]
  >> "%DST%" TYPE "%SRC%"
  COPY "%DST%" "ReadMe-Forum.txt"
  call stripbbcode.bat "%DST%"
  EXIT /B

:UPDATE_APPCONFIG
  replace.vbs "app.config" "<value>.?\d+\.\d+\.\d+.*</value>" "<value>v%VERSION%</value>"
  EXIT /B

:UPDATE_ASSEMBLY
  replace.vbs "My Project\AssemblyInfo.vb" "\d+\.\d+\.\d+\.\d+" "%VER_MAJOR%.%VER_MINOR%.%VER_PATCH%.%REVISION%"
  EXIT /B
